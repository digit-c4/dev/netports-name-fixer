# Netports name fixer

## Installation
1. On lab machines → **Do not use yet, Scrat will most likely timeout and return a 500 error**
    ```sh
    git clone https://code.europa.eu/digit-c4/dev/netports-name-fixer.git
    cd netports-name-fixer
    poetry install
    source .venv/bin/activate
    cp netports-name-fixer/.env.example netports-name-fixer/.env
    # Fill in the correct values in .env
    ```
2. On vworkers
    ```sh
    git clone https://code.europa.eu/digit-c4/dev/netports-name-fixer.git
    cd netports-name-fixer
    python3 -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    cp netports-name-fixer/.env.example netports-name-fixer/.env
    # Fill in the correct values in .env
    ```

## Usage
```sh
git clone https://code.europa.eu/digit-c4/dev/netports-name-fixer.git
cd netports-name-fixer/netports-name-fixer
source {venv_path}/bin/activate

# Backup the current netports and netplugs patching configurations
python netports_name_fixer/main.py --env prod backup ./configs spa3200-as03%

# Create demo data
python netports_name_fixer/main.py --env dev create_demo_data ./configs

# Test restore script on demo data
python netports_name_fixer/main.py --env dev restore ./configs spa3200-as03% SPA3.2.0.0.%

# Restore netports and netplugs patching configurations
python netports_name_fixer/main.py --env prod restore ./configs spa3200-as03% SPA3.2.0.0.%
```
