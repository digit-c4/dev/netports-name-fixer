certifi==2024.2.2
charset-normalizer==3.3.2
idna==3.6
python-dotenv==0.21.1
python-nms-common @ git+https://code.europa.eu/digit-c4/dev/python-nms-common.git@19559a5cfffe9349e0fd1ae0d3e40481b86eca27
requests==2.31.0
urllib3==2.0.7
