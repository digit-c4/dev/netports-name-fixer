#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, invalid-name
"""
Simple example on how to use Pydantic
"""
from typing import Optional, Union
from pathlib import Path
from json import load as json_load, dumps as json_dumps
from pydantic import BaseModel, Field


GET_ERROR = "GET_ERROR"


class SidModel(BaseModel):
    """Sid Model"""
    def get_properties(self) -> dict:
        """Serialize object into dict and remove problematic keys"""
        properties = self.model_dump(by_alias=True)
        del properties["value"]
        del properties["type"]
        return properties


class NetportModel(SidModel):
    """Netport Model"""
    has_as_name: Optional[str] = Field(alias="has as name")
    has_as_port_security_mac: Optional[str] = Field(alias="has as port-security mac")
    has_as_qos_profile: Optional[str] = Field(alias="has as qos profile")
    has_as_voice_vlan: Optional[str] = Field(alias="has as voice vlan")
    has_vlan: Optional[str] = Field(alias="has vlan")
    is_connected_to: Optional[str] = Field(alias="is connected to")
    type: str
    value: str


class NetplugModel(SidModel):
    """Netlug Model"""
    is_located_in: list = Field(alias="is located in")
    is_located_in_L_side: Optional[list] = Field(alias="is located in L side")
    is_connected_to: Optional[str] = Field(alias="is connected to")
    type: str
    value: str


def pretty_format(data) -> str:
    """Format a dictionary into a pretty string"""
    return json_dumps(data, sort_keys=True, indent=2, default=lambda x: x.model_dump())


def load_model_json_array(file_path: Path, model: Union[NetportModel, NetplugModel]) -> list:
    """Loads a JSON array from a file"""
    with open(file_path, "r", encoding="utf8") as f_in:
        json = json_load(f_in)
    return [model(**obj) for obj in json]


def get_netplug_model_from_dict(d: dict) -> NetplugModel:
    """Extracts a netplugs model from a dictionary"""
    netplug = {}
    building = d.get("Building", [{}])[0].get("value", GET_ERROR)
    floor = d.get("Floor (space)", [{}])[0].get("value", GET_ERROR)
    room = d.get("Room", [{}])[0].get("value", GET_ERROR)
    rack = d.get("Computer rack", [{}])[0].get("value", GET_ERROR)
    netplug["is located in L side"] = d.get("is located in L side", GET_ERROR)
    netplug["is connected to"] = d.get("is connected to", GET_ERROR)
    netplug["type"] = d.get("type", GET_ERROR)
    netplug["value"] = d.get("value", GET_ERROR)
    netplug["is located in"] = [building, floor, room, rack]
    return NetplugModel(**netplug)
