# pylint: disable=line-too-long, missing-function-docstring
"""
This module contains miscelaneous utilities for the netports_name_fixer project
"""
# Stdlib
from os import getenv
from sys import exit as sys_exit
from typing import Union, List
from socket import getfqdn
from json import dump as json_dump, dumps as json_dumps, load as json_load
from pathlib import Path
from argparse import Namespace as ArgsNamespace
from logging import INFO

# Third-party libraries
from dotenv import load_dotenv
from nms_common import nms_logging
from nms_common.diego import Diego
from nms_common.scrat import Scrat

# Project libraries
from libs.models import NetportModel, NetplugModel


# Load environment variables from .env
load_dotenv()

# Constants
ENV_VAR_NOT_SET_ERROR = "ENV_VAR_NOT_SET_ERROR"
PROD_HOST = getenv("PROD_HOST", ENV_VAR_NOT_SET_ERROR)
ACC_HOST = getenv("ACC_HOST", ENV_VAR_NOT_SET_ERROR)
DEV_HOST = getenv("DEV_HOST", ENV_VAR_NOT_SET_ERROR)
SID_FQDN = getenv("SID_FQDN", ENV_VAR_NOT_SET_ERROR)
SID_CONTEXT = getenv("SID_CONTEXT", ENV_VAR_NOT_SET_ERROR)
NETPORTS_TO_FIX_NAME_REGEX = r"\d{2}\/\d\/\d{2}"

class ScratError(Exception):
    """Generic error for Scrat operations"""

# Globals
logger = nms_logging.getAndConfigureLogger(owner_name="ntx", app_name=__name__, level=INFO)
env_to_fqdn_map = {
    "prod": PROD_HOST,
    "acc": ACC_HOST,
    "dev": DEV_HOST
}


def get_scrat_error_string(scrat_error: dict) -> str:
    """Extract the error string from a scrat error"""
    return scrat_error.args[1].get("error").split(".")[0].lower()


def pretty_format(data) -> str:
    """Format a dictionary into a pretty string"""
    return json_dumps(data, sort_keys=True, indent=2)


def get_sid_fqdn_from_args(args: ArgsNamespace) -> Union[str, None]:
    """Gets the FQDN to use with Oracle SID based on arguments invoked when starting the script"""
    return env_to_fqdn_map.get(args.env, None)


def get_sid_fqdn_from_conf() -> Union[str, None]:
    """Gets the Oracle SID FQDN from the local config in /opt/nCheck/bin/connect_avg_locations/config.json"""
    env = SID_FQDN
    return env_to_fqdn_map.get(env, None)


def get_sid_fqdn(args: ArgsNamespace) -> str:
    """Gets the FQDN to use with Oracle SID"""
    args_fqdn = get_sid_fqdn_from_args(args)
    conf_fqdn = get_sid_fqdn_from_conf()
    # Priority: args > conf > auto
    if args_fqdn:
        return args_fqdn
    if conf_fqdn:
        return conf_fqdn
    return getfqdn()


def load_config(load_dir: Path, config_name: str, model: Union[NetportModel, NetplugModel]) -> List[Union[NetportModel, NetplugModel]]:
    """Loads a single config file"""
    configs = []
    load_file_path = load_dir / f"{config_name}.json"
    logger.info("Loading %s config from %s", config_name, load_file_path)
    with open(load_file_path, "r", encoding="utf-8") as f_in:
        configs = json_load(f_in)
    return [model(**c) for c in configs]


def save_config(save_dir: Path, config_name: str, config_list: List[Union[NetportModel, NetplugModel]]) -> None:
    """Saves a single config file"""
    save_file_path = save_dir / f"{config_name}.json"
    logger.info("Saving %s config to %s", config_name, save_file_path)
    with open(save_file_path, "w", encoding="utf-8") as f_out:
        json_dump([c.model_dump(by_alias=True) for c in config_list], f_out, sort_keys=True, indent=2)


def init_dir(path: Path, user_choice_message: str) -> None:
    """Initialize a directory"""
    user_choice = input(user_choice_message)
    if user_choice.lower() != "y":
        sys_exit("Operation cancelled by user")
    path.mkdir(parents=True, exist_ok=True)


def diego_query(diego: Diego, dieget: str, params: dict) -> dict:
    """
    Gets all the connected SEP netports that have their name like 'pattern'

    Returns:
        dict: the response of the diego query
    """
    logger.debug("Dieget: %s", dieget)
    _, diego_response = diego.diego_run_dieget_by_name(
        dieget_name=dieget,
        param=params,
        Full=True
    )
    logger.debug("Done!")
    logger.debug("Diego results:")
    logger.debug(pretty_format(diego_response))
    return diego_response


def create_sid_object(scrat_instance: Scrat, name: str, sid_type: str, properties: dict) -> tuple:
    """
    Creates a new object in SID

    Args:
        scrat_instance:     scrat object instance to query SID
        name:               the object name
        sid_type:           the object's type in SID
        properties:         the object properties

    Returns:
        response, status_code   recieved from the Scrat API

    Raises:
        ScratError:             the scrat instance returned a status code indicating an error
    """
    response, status_code = scrat_instance.scratQuery({
        "context": SID_CONTEXT,
        "name": name,
        "type": sid_type,
        "properties": properties,
    }, query_mode="creation", Full=True)
    if status_code != 200:
        raise ScratError(f"Scrat failed with the following status code: {status_code}", response, status_code)
    return response, status_code


def update_sid_object(scrat_instance: Scrat, name: str, properties: object, sid_type: str, context: any) -> tuple:
    """
    Updates one object in SID

    Args:
        scrat_instance: scrat object instance to query SID
        name:           the object name
        properties:     the object properties
        sid_type:       the object sid type
        context:        the object context

    Returns:
        response, status_code   recieved from the Scrat API

    Raises:
        ScratError:             the scrat instance returned a status code indicating an error
    """
    response, status_code = scrat_instance.scratQuery({
        "context": context,
        "name": name,
        "type": sid_type,
        "properties": properties,
    }, query_mode="update", overwrite_mode="exclusive", line_only=False, Full=True)
    return response, status_code
