#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, missing-function-docstring
"""
usage: python main.py [-h] [--env [{dev,acc,prod}]] [-v]
                           {backup,create_demo_data,restore} ...

This script finds all the connected netports that share the same user-provided
pattern in their names and records them in save_dir. Does the same for
netplugs which have the same pattern in their 'is connected to' field.

positional arguments:
  {backup,create_demo_data,restore}

optional arguments:
  -h, --help            show this help message and exit
  --env [{dev,acc,prod}]
                        Set the desired env. Takes precedence on the local
                        .env file.
  -v, --verbose         Increase output's verbosity.
"""
# Stdlib
from os import getenv
from re import compile as re_compile
from sys import exit as sys_exit
from argparse import ArgumentParser, Namespace as ArgsNamespace
from logging import INFO, DEBUG
from pathlib import Path
from typing import List, Union

# Third-party libraries
from urllib3 import disable_warnings as urllib3_disable_warnings, exceptions as urllib3_exceptions
from dotenv import load_dotenv
from nms_common import nms_logging
from nms_common.diego import Diego
from nms_common.scrat import Scrat

# Project libraries
from libs.utils import ScratError, create_sid_object, get_scrat_error_string, get_sid_fqdn, init_dir, save_config, diego_query, load_config, update_sid_object   # pylint: disable=import-error
from libs.models import NetplugModel, NetportModel, get_netplug_model_from_dict

# Load environment variables from .env
load_dotenv()

# Constants
ENV_VAR_NOT_SET_ERROR = "ENV_VAR_NOT_SET_ERROR"
GET_ERROR = "GET_ERROR"
SID_FQDN = getenv("SID_FQDN", None)
SID_CONTEXT = getenv("SID_CONTEXT", ENV_VAR_NOT_SET_ERROR)
SID_USER = getenv("SID_USER", ENV_VAR_NOT_SET_ERROR)
SID_USERGROUP = getenv("SID_USERGROUP", ENV_VAR_NOT_SET_ERROR)
SID_PASSWORD = getenv("SID_PASSWORD", ENV_VAR_NOT_SET_ERROR)
GET_FULL_CONNECTED_NETPORTS_BY_PATTERN_DIEGET = getenv("GET_FULL_CONNECTED_NETPORTS_BY_PATTERN_DIEGET", ENV_VAR_NOT_SET_ERROR)
GET_FULL_NETPLUGS_BY_CONNECTED_TO_PATTERN_DIEGET = getenv("GET_FULL_NETPLUGS_BY_CONNECTED_TO_PATTERN_DIEGET", ENV_VAR_NOT_SET_ERROR)
GET_FULL_NETPORTS_BY_PATTERN_DIEGET = getenv("GET_FULL_NETPORTS_BY_PATTERN_DIEGET", ENV_VAR_NOT_SET_ERROR)
GET_FULL_NETPLUGS_BY_PATTERN_DIEGET = getenv("GET_FULL_NETPLUGS_BY_PATTERN_DIEGET", ENV_VAR_NOT_SET_ERROR)
NETPORTS_TO_FIX_NAME_REGEX = r"\d{2}\/\d\/\d{2}"

# Globals
netports_to_fix_regex = re_compile(NETPORTS_TO_FIX_NAME_REGEX)
logger = nms_logging.getAndConfigureLogger(owner_name="ntx", app_name=__name__, level=INFO)


def parse_script_args() -> ArgsNamespace:
    """
    Parse the script's arguments

    Returns:
        parsed_arguments
    """
    parser = ArgumentParser(
        prog="python main.py",
        description = "This script finds all the connected netports that share the same user-provided pattern in their names and records them in save_dir. Does the same for netplugs which have the same pattern in their 'is connected to' field."
    )
    parser.add_argument("--env", default=False, help="Set the desired env. Takes precedence on the local .env file.", dest="env", nargs="?", choices=("dev", "acc", "prod"))
    parser.add_argument("-v", "--verbose", action="store_true", default=False, help="Increase output's verbosity.", dest="verbose")
    subparsers = parser.add_subparsers(dest="subcommand")
    subparsers.required = True

    # Subparser for backup
    parser_backup = subparsers.add_parser("backup")
    parser_backup.add_argument("save_path", help="Which directory will be used to save the netports and netplugs configurations.")
    parser_backup.add_argument("netport_pattern", help="Pattern used to filter netports (eg. spa3200-as03%%).")

    # Subparser for creating demo data
    parser_create_demo_data = subparsers.add_parser("create_demo_data")
    parser_create_demo_data.add_argument("load_path", help="Which directory will be used to load the netports and netplugs configurations.")
    parser_create_demo_data.add_argument("-y", action="store_true", default=False, help="Skip user verification and proceed with current .env and args parameters (dangerous).", dest="skip")

    # Subparser for restore
    parser_restore = subparsers.add_parser("restore")
    parser_restore.add_argument("load_path", help="Which directory will be used to load the netports and netplugs configurations.")
    parser_restore.add_argument("ports_pattern", help="Pattern used to filter netports (eg. spa3200-as03%%).")
    parser_restore.add_argument("plugs_pattern", help="Pattern used to filter netplugs (eg. SPA3.2.0.0.%%).")

    args = parser.parse_args()
    return args


def save_configs(save_dir: Path, netports_configs: dict, netplugs_configs: dict) -> None:
    """Saves the current netports and netplugs configs to save_dir"""
    save_config(save_dir, "netports", netports_configs)
    save_config(save_dir, "netplugs", netplugs_configs)


class Main():
    """Main"""

    def __init__(self) -> None:
        self.netports_configs = {}
        self.netplugs_configs = {}
        self.args = parse_script_args()
        self.sid_fqdn = get_sid_fqdn(self.args)
        self._subcommand = self.args.subcommand
        self.diego = Diego(fqdn=self.sid_fqdn, verify=False, user=SID_USER, group=SID_USERGROUP, password=SID_PASSWORD, verbose=self.args.verbose)
        self.scrat = Scrat(fqdn=self.sid_fqdn, verify=False, user=SID_USER, group=SID_USERGROUP, password=SID_PASSWORD, version=2, verbose=self.args.verbose)
        # Increase verbosity if requested
        if self.args.verbose:
            logger.info("Using maximum verbosity")
            logger.setLevel(DEBUG)
        # Make sure the user is okay with that FQDN
        user_choice = input(f"Using the following Diego FQDN: {self.sid_fqdn}. Ok (y/n)? ")
        if user_choice.lower() != "y":
            sys_exit("Operation cancelled by user")

    @property
    def subcommand(self):
        """User-selected subcommand"""
        return self._subcommand

    def create_objects_from_configs(self, configs: List[Union[NetportModel, NetplugModel]]) -> None:
        """Creates objects in sid from a list of configs"""
        for obj in configs:
            properties = obj.get_properties()
            logger.info("Creating object %s (%s)", obj.value, obj.type)
            try:
                # TODO: test this
                response, status_code = create_sid_object(self.scrat, obj.value, obj.type, properties)
            except ScratError as scrat_error:
                error_message = get_scrat_error_string(scrat_error)
                status_code = scrat_error.args[2]
                logger.error("The requests failed with the following message: %s (status code: %s)", error_message, status_code)
            else:
                logger.info("Done! New SID item: %s", response)

    def patch_objects_using_configs(self, filtered_sid_objects: List[Union[NetportModel, NetplugModel]], objects_configs: List[Union[NetportModel, NetplugModel]], patched_object_name: str, target_patch_name: str) -> None:
        number_of_patched_objects = 0
        number_of_objects_to_patch = len(filtered_sid_objects)
        logger.info("Found %s %ss to patch!", number_of_objects_to_patch, patched_object_name)
        logger.info("Patching %ss 'is connected to' fields...", patched_object_name)
        for sid_object in filtered_sid_objects:
            if isinstance(sid_object, NetportModel):
                object_config = next((n for n in objects_configs if n.value.replace("/0", "/1/0") == sid_object.value), None)
            elif isinstance(sid_object, NetplugModel):
                object_config = next((n for n in objects_configs if n.value == sid_object.value), None)
            if object_config is None:
                logger.error("Cannot find %s %s properties", patched_object_name, sid_object.value)
                continue
            # Replacing old names by the correct ones
            if isinstance(sid_object, NetplugModel):
                object_config.is_connected_to = object_config.is_connected_to.replace("/0", "/1/0")
            logger.info("Patching %s %s to %s %s", patched_object_name, sid_object.value, target_patch_name, object_config.is_connected_to)
            response, status_code = update_sid_object(self.scrat, sid_object.value, {"is connected to": object_config.is_connected_to}, sid_object.type, SID_CONTEXT)
            if status_code == 200:
                logger.info("Done! Response: %s - Status code: %s", response, status_code)
                number_of_patched_objects += 1
            else:
                logger.error("Patching failed with the following status code: %d - API response: %s", status_code, response)

        logger.info("Patched %d/%d %ss!", number_of_patched_objects, number_of_objects_to_patch, patched_object_name)

    @staticmethod
    def prepare_netports_demo_data(netports_configs: List[NetportModel]) -> list:
        """Simulate re-discovery by using the correct names for netports (eg. spa3200-as03/1/0/40_Gi instead of spa3200-as03/0/40_Gi) and resetting the 'is connected to' field"""
        return [
            NetportModel(**{
                "has as name": n.has_as_name,
                "has as port-security mac": n.has_as_port_security_mac,
                "has as qos profile": n.has_as_qos_profile,
                "has as voice vlan": n.has_as_voice_vlan,
                "has vlan": n.has_vlan,
                "is connected to": None,
                "type": n.type,
                "value": n.value.replace("/0", "/1/0")
            }) for n in netports_configs
        ]

    @staticmethod
    def prepare_netplugs_demo_data(netplugs_configs: List[NetplugModel]) -> list:
        """Simulate re-discovery by using the correct names for netplugs (eg. spa3200-as03/1/0/40_Gi instead of spa3200-as03/0/40_Gi) and resetting the 'is connected to' field"""
        return [
            NetplugModel(**{
                "is located in": n.is_located_in,
                "is located in L side": n.is_located_in_L_side,
                "is connected to": None,
                "type": n.type,
                "value": n.value.replace("/0", "/1/0")
            }) for n in netplugs_configs
        ]

    def backup(self) -> None:
        """Backup netports and netplugs configs"""
        # Initialize save dir
        save_path = Path(self.args.save_path).resolve()
        init_dir(save_path, f"Will save netports and netplugs configs in: {save_path}. Ok (y/n)? ")

        # Getting netports from SID
        logger.info("Getting netports using %s as name pattern and not null 'is connected to' field...", self.args.netport_pattern)
        response = diego_query(self.diego, GET_FULL_CONNECTED_NETPORTS_BY_PATTERN_DIEGET, {"pattern": self.args.netport_pattern})
        netports = response.get("results", {})
        logger.info("Netports list downloaded!")

        # Filtering netports to keep only those who have a name pattern matching dd/d/dd (where d is a single digit)
        logger.info("Searching netports to fix using %s as regex on netports list", NETPORTS_TO_FIX_NAME_REGEX)
        filtered_netports = [NetportModel(**netport_dict) for name, netport_dict in netports.items() if netports_to_fix_regex.search(name)]
        logger.info("Found %s netports to fix...", len(filtered_netports))
        netports_configs = filtered_netports

        # Getting netplugs from SID
        logger.info("Getting netplugs using %s as pattern for their 'is connected to' field...", self.args.netport_pattern)
        response = diego_query(self.diego, GET_FULL_NETPLUGS_BY_CONNECTED_TO_PATTERN_DIEGET, {"pattern": self.args.netport_pattern})
        netplugs = response.get("results", {})
        logger.info("Netplugs list downloaded!")
        filtered_netports_names = [n.value for n in filtered_netports]
        filtered_netplugs = [get_netplug_model_from_dict(netplug_dict) for _, netplug_dict in netplugs.items() if netplug_dict.get("is connected to", GET_ERROR) in filtered_netports_names]
        netplugs_configs = filtered_netplugs

        # Serializing configs into JSON
        save_configs(save_path, netports_configs, netplugs_configs)

    def create_demo_data(self) -> None:
        """Create demo data"""
        # Load configs from load_path
        load_path = Path(self.args.load_path).resolve()
        if not self.args.skip:
            user_choice = input(f"Netports and netplugs patching configs will be loaded from: {load_path}. Ok (y/n)? ")
            if user_choice.lower() != "y":
                sys_exit("Operation cancelled by user")
        netports_configs = load_config(load_path, "netports", NetportModel)
        netplugs_configs = load_config(load_path, "netplugs", NetplugModel)

        # Prepares netports demo data
        netports_demo_data = self.prepare_netports_demo_data(netports_configs)

        # Prepares netplugs demo data
        netplugs_demo_data = self.prepare_netplugs_demo_data(netplugs_configs)

        # Creating netports demo data
        self.create_objects_from_configs(netports_demo_data)

        # Creating netplugs demo data
        self.create_objects_from_configs(netplugs_demo_data)

    def restore(self) -> None:
        """Restore netports and netplugs configs"""
        # Load configs from load_path
        load_path = Path(self.args.load_path).resolve()
        user_choice = input(f"Netports and netplugs patching configs will be loaded from: {load_path}. Ok (y/n)? ")
        if user_choice.lower() != "y":
            sys_exit("Operation cancelled by user")
        netports_configs = load_config(load_path, "netports", NetportModel)
        netplugs_configs = load_config(load_path, "netplugs", NetplugModel)
        netports_names_from_cfg = [n.value.replace("/0", "/1/0") for n in netports_configs]
        netplugs_names_from_cfg = [n.value for n in netplugs_configs]

        # Getting netports from SID
        logger.info("Getting netports to patch using %s as name pattern...", self.args.ports_pattern)
        response = diego_query(self.diego, GET_FULL_NETPORTS_BY_PATTERN_DIEGET, {"pattern": self.args.ports_pattern})
        sid_netports = response.get("results", {})
        logger.info("List of netports to patch downloaded!")

        # Filtering netports to keep only those who have a backup config in self.args.load_path
        logger.info("Searching which netports to patch by filtering out previously unpatched netports from the downloaded netports list...")
        filtered_sid_netports = [NetportModel(**v) for k, v in sid_netports.items() if k in netports_names_from_cfg]

        # Patching netports 'is connected to' fields TODO: test
        self.patch_objects_using_configs(filtered_sid_netports, netports_configs, "netport", "netplug")

        # Getting netplugs from SID
        logger.info("Getting netplugs using %s as name pattern...", self.args.plugs_pattern)
        response = diego_query(self.diego, GET_FULL_NETPLUGS_BY_PATTERN_DIEGET, {"pattern": self.args.plugs_pattern})
        sid_netplugs = response.get("results", {})
        logger.info("Netplugs list downloaded!")

        # Filtering netplugs to keep only those who have a backup config in args.load_path
        logger.info("Searching which netplugs to patch by filtering out previously unpatched netplugs from the downloaded netplugs list...")
        filtered_sid_netplugs = [get_netplug_model_from_dict(v) for k, v in sid_netplugs.items() if k in netplugs_names_from_cfg]

        # Patching netplugs 'is connected to' fields TODO: test
        self.patch_objects_using_configs(filtered_sid_netplugs, netplugs_configs, "netplug", "netport")


if __name__ == "__main__":
    # Disable all 'insecure request' warnings
    urllib3_disable_warnings(urllib3_exceptions.InsecureRequestWarning)
    main = Main()
    if main.subcommand == "backup":
        main.backup()
    elif main.subcommand == "create_demo_data":
        main.create_demo_data()
    elif main.subcommand == "restore":
        main.restore()
    else:
        raise RuntimeError(f"Unknown subcommand \'{main.subcommand}\'")
